
import Foundation

class DateFunctions {
    
    class func formatDate() -> String {
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.locale = NSLocale.currentLocale()
        dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
        let convertedDate = dateFormatter.stringFromDate(date)
        return convertedDate
        
    }
    
    class func lastDays(nrOfDays: Int) -> NSDate {
        
        let userCalendar = NSCalendar.currentCalendar()
        
        let periodComponents = NSDateComponents()
        periodComponents.day = nrOfDays
        let nrOfDays = userCalendar.dateByAddingComponents(
            periodComponents,
            toDate: NSDate(),
            options: [])!
        
        return nrOfDays
    }
    
    func foodsEatenThatDay() {
        
    }
    
}
