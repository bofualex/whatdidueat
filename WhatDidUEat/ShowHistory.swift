
import Foundation
import UIKit

class ShowHistory: UITableViewController, FoodManagerDelegate {
    
    var history: [FoodTypes] = []
    let manager = FoodManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 36.0
        tableView.rowHeight = UITableViewAutomaticDimension
        manager.delegate = self
        manager.requestFood(nil)
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "HistoryCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ShowHistoryCell
        cell.activityIndicator.startAnimating()
        cell.whatWasEaten.text = history[indexPath.row].todayMeal
        cell.mealDate.text = history[indexPath.row].dataMesei
        cell.activityIndicator.stopAnimating()
        return cell
    }
    
    //MARK: Action methods
    
    @IBAction func showActionSheetTapped(sender: AnyObject) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Filter history by: ", message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        
        for i in 0..<FoodTypes.foodType.count {
            
            let general: UIAlertAction = UIAlertAction(title: FoodTypes.foodType[i], style: .Default) { action -> Void in
                self.manager.requestFood(FoodTypes.foodType[i])
                self.tableView.reloadData()
            }
            actionSheetController.addAction(general)
        }
        
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
    
    //MARK: Delegate method
    
    func foodManagerDelegate (response: [FoodTypes]){
        history = manager.history
    }
}
