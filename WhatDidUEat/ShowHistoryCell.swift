
import Foundation
import UIKit

class ShowHistoryCell: UITableViewCell {
    
    @IBOutlet var mealDate: UILabel!
    @IBOutlet var whatWasEaten: UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
