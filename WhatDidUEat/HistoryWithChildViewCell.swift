
import Foundation
import UIKit

class HistoryWithChildViewCell: UITableViewCell {
    
    @IBOutlet var mealDate: UILabel!
    @IBOutlet var whatWasEaten: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}