
import Foundation
import UIKit
import Charts

class BarChart: UIViewController, ChartViewDelegate, FoodManagerDelegate {
    
    let manager = FoodManager()
    var chartValues: [FoodTypes]!
    
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var pieChartView: PieChartView!
    
    @IBOutlet var activityIndicator1: UIActivityIndicatorView!
    @IBOutlet var activityIndicator2: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        activityIndicator1.startAnimating()
        activityIndicator2.startAnimating()
        
        manager.delegate = self
        manager.requestFood(nil)
        barChartView.delegate = self
        loadCharts()
        activityIndicator1.stopAnimating()
        activityIndicator2.stopAnimating()
    }
    
    func loadCharts() {
        GraphFunctions.setChart(FoodTypes.foodType, values: manager.foodTypeCount(), barChartView: barChartView)
        GraphFunctions.setPieChart(FoodTypes.foodType, values: manager.foodTypeCount(), pieChartView: pieChartView)
    }
    
    //MARK: Action methods
    
    @IBAction func saveBarChart(sender: UITapGestureRecognizer) {
        let refreshAlert = UIAlertController(title: "Save bar chart", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            self.barChartView.saveToCameraRoll()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
        }))
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func savePieChart(sender: UITapGestureRecognizer){
        let refreshAlert = UIAlertController(title: "Save pie chart", message: nil, preferredStyle: UIAlertControllerStyle.Alert)
        
        refreshAlert.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            self.pieChartView.saveToCameraRoll()
        }))
        
        refreshAlert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) in
        }))
        
        presentViewController(refreshAlert, animated: true, completion: nil)
    }
    
    @IBAction func saveCharts(sender: UIBarButtonItem){

            let actionSheetController: UIAlertController = UIAlertController(title: "Save: ", message: nil, preferredStyle: .ActionSheet)
            
            let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
            }
            actionSheetController.addAction(cancelAction)
            
            let saveBar: UIAlertAction = UIAlertAction(title: "Save bar chart", style: .Default) { action -> Void in
                self.barChartView.saveToCameraRoll()
            }
            actionSheetController.addAction(saveBar)

            let savePie: UIAlertAction = UIAlertAction(title: "Save pie chart", style: .Default) { action -> Void in
                self.pieChartView.saveToCameraRoll()
                }
                actionSheetController.addAction(savePie)
            
            let saveBoth: UIAlertAction = UIAlertAction(title: "Save both charts", style: .Default) { action -> Void in
                self.pieChartView.saveToCameraRoll()
                self.barChartView.saveToCameraRoll()
            }
            actionSheetController.addAction(saveBoth)
            self.presentViewController(actionSheetController, animated: true, completion: nil)
        }

    
    //MARK: Delegate methods
    
    
    func foodManagerDelegate (response: [FoodTypes]){
        chartValues = manager.history
    }
}