

import Foundation
import UIKit

class CalendarView: UIViewController, FoodManagerDelegate {
    
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var foodHistoryLabel: UILabel!
    
    let manager = FoodManager()
    var history: [FoodTypes]! = []
    var foodPerDate: [String]! = []
    var mealDate: String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
        datePicker.maximumDate = NSDate()
        datePicker.addTarget(self, action: #selector(retrieveDate(_:)), forControlEvents: UIControlEvents.ValueChanged)
    }
    
    //MARK: action method
    
    func retrieveDate(sender: UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.FullStyle
        mealDate = dateFormatter.stringFromDate(datePicker.date)
        manager.requestDate(mealDate)
        foodHistoryLabel.text = ""
        for i in 0..<history.count {
            foodHistoryLabel.text = foodHistoryLabel.text! + String("\n") + history[i].todayMeal!
        }
        foodPerDate.removeAll()
    }
    
    //MARK: delegate method
    
    func foodManagerDelegate (response: [FoodTypes]) {
        history = manager.history
    }
}


