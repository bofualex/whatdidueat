import Foundation
import UIKit
import DLRadioButton

class AddChildView: UIViewController {

    @IBOutlet weak var fruitButton : DLRadioButton!
    @IBOutlet weak var cerealButton : DLRadioButton!
    @IBOutlet weak var dairyButton : DLRadioButton!
    @IBOutlet weak var soupButton : DLRadioButton!
    var selectedFoods: [String]?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        selected()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func selected() {
        self.fruitButton.multipleSelectionEnabled = true
        self.cerealButton.multipleSelectionEnabled = true
        self.dairyButton.multipleSelectionEnabled = true
        self.soupButton.multipleSelectionEnabled = true
    }
    
//MARK: action methods
    
    @objc @IBAction private func logSelectedButton(radioButton : DLRadioButton) {
        if (radioButton.multipleSelectionEnabled) {
            for button in radioButton.selectedButtons() {
            print(String(format: "%@ is selected.\n", button.titleLabel!.text!))
            }
        } else {
            print(String(format: "%@ is selected.\n", radioButton.selectedButton()!.titleLabel!.text!))
            selectedFoods?.append(radioButton.selectedButton()!.titleLabel!.text!)
            print(selectedFoods)
        }
    }
    
    @IBAction func save(sender: UIButton) {
        
        self.willMoveToParentViewController(nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
    
    @IBAction func cancel(sender: UIButton) {
        self.willMoveToParentViewController(nil)
        self.view.removeFromSuperview()
        self.removeFromParentViewController()
    }
}
