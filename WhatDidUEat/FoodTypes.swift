
import Foundation
import CoreData

class FoodTypes: NSManagedObject{
    
    @NSManaged var dataMesei: String?
    @NSManaged var todayMeal: String?
    
    static let foodType: [String]! = ["Fruit","Cereal","Dairy","Soup"]
        
    
}

