
import Foundation
import UIKit

class AddViewController: UIViewController {
    
    let manager = FoodManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: Action methods
    
    @IBAction func showActionSheetTapped(sender: AnyObject) {
        
        let actionSheetController: UIAlertController = UIAlertController(title: "Add today meal: ", message: nil, preferredStyle: .ActionSheet)
        
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .Cancel) { action -> Void in
        }
        actionSheetController.addAction(cancelAction)
        
        for i in 0..<FoodTypes.foodType.count {
            
            let general: UIAlertAction = UIAlertAction(title: FoodTypes.foodType[i], style: .Default) { action -> Void in
                self.manager.addFood(FoodTypes.foodType[i])
            }
            actionSheetController.addAction(general)
        }
        
        self.presentViewController(actionSheetController, animated: true, completion: nil)
    }
    
}

