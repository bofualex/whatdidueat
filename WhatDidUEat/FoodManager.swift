
import Foundation
import UIKit
import CoreData

@objc protocol FoodManagerDelegate: class {
    optional func foodManagerDelegate (response: [FoodTypes])
}


class FoodManager: NSObject {
    
    var history: [FoodTypes]!
    var todayFood: FoodTypes!
    
    let groupRequest = dispatch_group_create()
    
    weak var delegate: FoodManagerDelegate?
    
    let managedObjectContext = (UIApplication.sharedApplication().delegate as? AppDelegate)?.managedObjectContext
    let fetchRequest = NSFetchRequest(entityName: "FoodTypes")
    var predicate: NSPredicate?
    
    
    //MARK: CoreData methods
    
    func addFood(masaZilei: String) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),  { [weak self] in
            self!.todayFood = NSEntityDescription.insertNewObjectForEntityForName("FoodTypes", inManagedObjectContext: self!.managedObjectContext!) as! FoodTypes
            self!.todayFood.todayMeal = masaZilei
            self!.todayFood.dataMesei = DateFunctions.formatDate()
            do {
                try self!.managedObjectContext!.save()
            } catch {
                print(error)
            }
            })
    }
    
    func requestFood(foodkey: String?){
        if (foodkey != nil) {
            predicate = NSPredicate(format: "todayMeal == %@", foodkey!) }
        else {
            predicate = NSPredicate(format: "todayMeal.length > 0")
        }
        fetchRequest.predicate = predicate
        do {
            let results = try managedObjectContext!.executeFetchRequest(fetchRequest)
            history = results as! [FoodTypes]
        } catch
            let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
        }
        self.delegate?.foodManagerDelegate!(history)
    }
    
    func requestDate(foodkey: String?){
        if (foodkey != nil) {
            predicate = NSPredicate(format: "dataMesei == %@", foodkey!) }
        else {
            predicate = NSPredicate(format: "dataMesei.length > 0")
        }
        fetchRequest.predicate = predicate
        do {
            let results = try managedObjectContext!.executeFetchRequest(fetchRequest)
            history = results as! [FoodTypes]
        } catch
            let error as NSError {
                print("Could not fetch \(error), \(error.userInfo)")
        }
        self.delegate?.foodManagerDelegate!(history)
    }
    
    //MARK: Array methods
    
    func countElements(foodkey: String) -> Double {
        var arr: Double = 0
        //dispatch_group_enter(groupRequest)
        //dispatch_group_async(groupRequest, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),  { [weak self] in
        for i in 0..<self.history.count {
            if self.history[i].todayMeal == foodkey{
                arr += 1
            }
        }
        // })
        //dispatch_group_leave(groupRequest)
        return arr
    }
    
    func foodAlert() -> Bool{
        var bool: Bool!
        var arrNrOfElements: [Double]! = []
        self.requestFood("")
        for i in 0..<FoodTypes.foodType.count{
            arrNrOfElements.append(self.countElements(FoodTypes.foodType[i]))
        }
        if arrNrOfElements.maxElement()! > (arrNrOfElements.minElement()! + 2.0) {
            bool = true
        } else {
            bool = false
        }
        return bool
    }
    
    func foodTypeCount() -> [Double]{
        var entries: [Double]! = []
        //dispatch_group_enter(groupRequest)
        //dispatch_group_notify(groupRequest, dispatch_get_main_queue(), { [weak self] in
        for i in 0..<FoodTypes.foodType.count{
            entries.append(self.countElements(FoodTypes.foodType[i]))
        }
        //})
        //dispatch_group_leave(groupRequest)
        return entries
    }
}
    
    
    
    //    func requestHistory(foodkey: String?){
    //        var sortedFoods: [FoodTypes] = []
    //        let fetchRequest = NSFetchRequest(entityName: "FoodTypes")
    //        //        dispatch_group_enter(groupRequest)
    //        //        dispatch_group_async(groupRequest, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0),  { [weak self] in
    //        do {
    //            let results = try managedObjectContext!.executeFetchRequest(fetchRequest)
    //            history = results as! [FoodTypes]
    //        } catch
    //            let error as NSError {
    //                print("Could not fetch \(error), \(error.userInfo)")
    //        }
    //        if foodkey == ""{
    //            sortedFoods = history}
    //        else{
    //            for item in (0..<history.count) {
    //                if history[item].todayMeal == foodkey {
    //                    sortedFoods.append(history[item])
    //                }
    //            }
    //        }
    //        history = sortedFoods
    //        self.delegate?.foodManagerDelegate!(history)
    //        //            })
    //        //        dispatch_group_leave(groupRequest)
    //        //        dispatch_async( dispatch_get_main_queue(), { [weak self] in
    //        //            self!.delegateFinishedGetting?.foodManagerFinishedGettingFood!()
    //        //            })
    //    }

