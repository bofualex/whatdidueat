import Foundation
import UIKit

class HistoryWithChildView: UITableViewController, FoodManagerDelegate {
    
    var history: [FoodTypes] = []
    let manager = FoodManager()
    @IBOutlet var contentView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.estimatedRowHeight = 36.0
        tableView.rowHeight = UITableViewAutomaticDimension
        manager.delegate = self
        manager.requestFood(nil)
        tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return history.count
        
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier = "HistoryWithChildCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! HistoryWithChildViewCell
        cell.whatWasEaten.text = history[indexPath.row].todayMeal
        cell.mealDate.text = history[indexPath.row].dataMesei
        return cell
    }
    
    //MARK: Action methods
    
    @IBAction func filter(sender: UIBarButtonItem) {
        let addChildView = self.storyboard?.instantiateViewControllerWithIdentifier("AddChildView") as! AddChildView
        self.addChildViewController(addChildView)
        self.view.addSubview((addChildView.view)!)
        
        let viewsDict = ["child" : addChildView.view]
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("H:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        view.addConstraints(NSLayoutConstraint.constraintsWithVisualFormat("V:|[child]|", options: NSLayoutFormatOptions(rawValue: 0), metrics: nil, views: viewsDict))
        
        addChildView.didMoveToParentViewController(self)
    }

    //MARK: Delegate method
    
    func foodManagerDelegate (response: [FoodTypes]){
        history = manager.history
    }
}
